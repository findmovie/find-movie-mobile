package com.itis.moviesearchapp

import android.app.Application
import com.itis.moviesearchapp.di.AppComponent
import com.itis.moviesearchapp.di.DaggerAppComponent

class App : Application() {

    companion object {
        lateinit var appComponent: AppComponent
    }

    override fun onCreate() {
        super.onCreate()
        initComponent()
    }

    private fun initComponent() {
        appComponent = DaggerAppComponent
            .builder()
            .bindContext(this)
            .build()
            .apply { inject(this@App) }
    }

}