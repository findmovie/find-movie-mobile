package com.itis.moviesearchapp.ui.notifications

import androidx.lifecycle.ViewModelProvider
import com.itis.moviesearchapp.R
import com.itis.moviesearchapp.base.BaseFragment
import com.itis.moviesearchapp.utils.observe
import kotlinx.android.synthetic.main.fragment_notifications.title

class NotificationsFragment : BaseFragment() {

    private lateinit var viewModel: NotificationsViewModel

    override val layoutId = R.layout.fragment_notifications

    override fun setupViews() {
        viewModel = ViewModelProvider(this).get(NotificationsViewModel::class.java)
    }

    override fun setupLiveData() {
        observe(viewModel.titleLiveData, title::setText)
    }
}