package com.itis.moviesearchapp.ui.home

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class HomeViewModel : ViewModel() {

    val titleLiveData = MutableLiveData("This is home Fragment")
}