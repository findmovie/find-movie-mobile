package com.itis.moviesearchapp.ui.notifications

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class NotificationsViewModel : ViewModel() {

    val titleLiveData = MutableLiveData("This is notifications Fragment")
}