package com.itis.moviesearchapp.ui.dashboard

import androidx.lifecycle.ViewModelProvider
import com.itis.moviesearchapp.R
import com.itis.moviesearchapp.base.BaseFragment
import com.itis.moviesearchapp.utils.observe
import kotlinx.android.synthetic.main.fragment_dashboard.title

class DashboardFragment : BaseFragment() {

    private lateinit var viewModel: DashboardViewModel

    override val layoutId = R.layout.fragment_dashboard

    override fun setupViews() {
        viewModel = ViewModelProvider(this).get(DashboardViewModel::class.java)
    }

    override fun setupLiveData() {
        observe(viewModel.titleLiveData, title::setText)
    }
}