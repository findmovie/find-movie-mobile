package com.itis.moviesearchapp.ui.home

import androidx.lifecycle.ViewModelProvider
import com.itis.moviesearchapp.R
import com.itis.moviesearchapp.base.BaseFragment
import com.itis.moviesearchapp.utils.observe
import kotlinx.android.synthetic.main.fragment_home.title

class HomeFragment : BaseFragment() {

    private lateinit var viewModel: HomeViewModel

    override val layoutId = R.layout.fragment_home

    override fun setupViews() {
        viewModel = ViewModelProvider(this).get(HomeViewModel::class.java)
    }

    override fun setupLiveData() {
        observe(viewModel.titleLiveData, title::setText)
    }
}