package com.itis.moviesearchapp.ui

import android.os.Bundle
import androidx.navigation.findNavController
import androidx.navigation.ui.NavigationUI
import com.itis.moviesearchapp.R
import com.itis.moviesearchapp.base.BaseActivity
import kotlinx.android.synthetic.main.activity_main.navView

class MainActivity : BaseActivity() {

    override val layoutId = R.layout.activity_main

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.Theme_App)
        super.onCreate(savedInstanceState)
    }

    override fun setupViews() {
        val navController = findNavController(R.id.navHostFragment)
        NavigationUI.setupWithNavController(navView, navController)
    }
}