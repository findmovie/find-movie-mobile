package com.itis.moviesearchapp.ui.dashboard

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class DashboardViewModel : ViewModel() {

    val titleLiveData = MutableLiveData("This is dashboard Fragment")
}