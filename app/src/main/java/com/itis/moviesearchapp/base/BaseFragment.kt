package com.itis.moviesearchapp.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.itis.moviesearchapp.utils.updateSystemBars

abstract class BaseFragment : Fragment() {

    interface OnBackPressedListener {
        fun onBackPressed()
    }

    protected abstract val layoutId: Int

    protected open val hasLightStatusBar = true
    protected open val hasLightNavigationBar = true

    protected open fun inject() {}

    protected open fun setupViews() {}

    protected open fun applyInsets() {}

    protected open fun setupLiveData() {}

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(layoutId, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        inject()
        applyInsets()
        setupViews()
        setupLiveData()
    }

    override fun onStart() {
        super.onStart()
        requireActivity().updateSystemBars(hasLightStatusBar, hasLightNavigationBar)
    }
}
