package com.itis.moviesearchapp.base

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

abstract class BaseActivity : AppCompatActivity() {

    protected abstract val layoutId: Int

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layoutId)
        inject()
        applyInsets()
        setupViews()
        setupLiveData()
    }

    protected open fun inject() {}

    protected open fun setupViews() {}

    protected open fun applyInsets() {}

    protected open fun setupLiveData() {}
}
