package com.itis.moviesearchapp.utils

import android.app.Activity
import android.content.res.Resources
import android.os.Build
import android.view.View
import android.view.Window
import androidx.core.content.res.ResourcesCompat
import com.itis.moviesearchapp.R

fun Activity.updateSystemBars(hasLightStatusBar: Boolean, hasLightNavigationBar: Boolean) {
    this.window.updateSystemBars(hasLightStatusBar, hasLightNavigationBar)
}

private fun Window.updateSystemBars(hasLightStatusBar: Boolean, hasLightNavigationBar: Boolean) {
    // set the content to appear under the system bars
    decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_STABLE or
        View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION or
        View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
    updateNavigationBar(hasLightNavigationBar, this.context.resources)
    updateStatusBar(hasLightStatusBar, this.context.resources)
}

private fun Window.updateStatusBar(hasLightStatusBar: Boolean, resources: Resources) {
    when {
        Build.VERSION.SDK_INT >= Build.VERSION_CODES.M -> {
            if (hasLightStatusBar) {
                decorView.systemUiVisibility = decorView.systemUiVisibility or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
                statusBarColor = ResourcesCompat.getColor(resources, R.color.white_70, null)
            } else {
                statusBarColor = ResourcesCompat.getColor(resources, R.color.black_20, null)
            }
        }
        Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP -> {
            // it is not possible to make buttons dark so color bar black
            statusBarColor = ResourcesCompat.getColor(resources, R.color.black_20, null)
        }
    }
}

private fun Window.updateNavigationBar(hasLightNavigationBar: Boolean, resources: Resources) {
    when {
        Build.VERSION.SDK_INT >= Build.VERSION_CODES.O -> {
            if (hasLightNavigationBar) {
                decorView.systemUiVisibility = decorView.systemUiVisibility or View.SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR
                navigationBarColor = ResourcesCompat.getColor(resources, R.color.white_70, null)
            } else {
                navigationBarColor = ResourcesCompat.getColor(resources, R.color.black_20, null)
            }
        }
        Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP -> {
            // it is not possible to make buttons dark so color bar black
            navigationBarColor = ResourcesCompat.getColor(resources, R.color.black_20, null)
        }
    }
}
